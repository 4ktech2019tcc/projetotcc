﻿namespace ChocoBunnyBy4KTeck__TCC_2019_.Telas
{
    partial class frmMenu
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            this.panel1 = new System.Windows.Forms.Panel();
            this.pictureBox1 = new System.Windows.Forms.PictureBox();
            this.PanelDosBtn = new System.Windows.Forms.FlowLayoutPanel();
            this.panelSuprimento = new System.Windows.Forms.Panel();
            this.bunifuFlatButton21 = new Bunifu.Framework.UI.BunifuFlatButton();
            this.bunifuFlatButton22 = new Bunifu.Framework.UI.BunifuFlatButton();
            this.bunifuFlatButton23 = new Bunifu.Framework.UI.BunifuFlatButton();
            this.btnSuprimentos = new Bunifu.Framework.UI.BunifuFlatButton();
            this.panelFinanceiro = new System.Windows.Forms.Panel();
            this.bunifuFlatButton2 = new Bunifu.Framework.UI.BunifuFlatButton();
            this.bunifuFlatButton3 = new Bunifu.Framework.UI.BunifuFlatButton();
            this.bunifuFlatButton4 = new Bunifu.Framework.UI.BunifuFlatButton();
            this.btnFinanceiro = new Bunifu.Framework.UI.BunifuFlatButton();
            this.PanelEstoque = new System.Windows.Forms.Panel();
            this.bunifuFlatButton1 = new Bunifu.Framework.UI.BunifuFlatButton();
            this.bunifuFlatButton18 = new Bunifu.Framework.UI.BunifuFlatButton();
            this.bunifuFlatButton19 = new Bunifu.Framework.UI.BunifuFlatButton();
            this.btnEstoque = new Bunifu.Framework.UI.BunifuFlatButton();
            this.panelRH = new System.Windows.Forms.Panel();
            this.bunifuFlatButton5 = new Bunifu.Framework.UI.BunifuFlatButton();
            this.bunifuFlatButton14 = new Bunifu.Framework.UI.BunifuFlatButton();
            this.bunifuFlatButton15 = new Bunifu.Framework.UI.BunifuFlatButton();
            this.btnRH = new Bunifu.Framework.UI.BunifuFlatButton();
            this.timerSuprimento = new System.Windows.Forms.Timer(this.components);
            this.timerFinanceiro = new System.Windows.Forms.Timer(this.components);
            this.timerEstoque = new System.Windows.Forms.Timer(this.components);
            this.timerRH = new System.Windows.Forms.Timer(this.components);
            this.panel5 = new System.Windows.Forms.Panel();
            this.pictureBox3 = new System.Windows.Forms.PictureBox();
            this.MunuTimer = new System.Windows.Forms.Timer(this.components);
            this.panel6 = new System.Windows.Forms.Panel();
            this.ptrLogo = new System.Windows.Forms.PictureBox();
            this.frmTeste1 = new ChocoBunnyBy4KTeck__TCC_2019_.Telas.frmTeste();
            this.frmCadastrarfuncionario1 = new ChocoBunnyBy4KTeck__TCC_2019_.Telas.funcionarios.frmCadastrarfuncionario();
            this.panel1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox1)).BeginInit();
            this.PanelDosBtn.SuspendLayout();
            this.panelSuprimento.SuspendLayout();
            this.panelFinanceiro.SuspendLayout();
            this.PanelEstoque.SuspendLayout();
            this.panelRH.SuspendLayout();
            this.panel5.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox3)).BeginInit();
            this.panel6.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.ptrLogo)).BeginInit();
            this.SuspendLayout();
            // 
            // panel1
            // 
            this.panel1.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(188)))), ((int)(((byte)(0)))), ((int)(((byte)(50)))));
            this.panel1.Controls.Add(this.pictureBox1);
            this.panel1.Location = new System.Drawing.Point(0, 0);
            this.panel1.MaximumSize = new System.Drawing.Size(262, 626);
            this.panel1.MinimumSize = new System.Drawing.Size(42, 626);
            this.panel1.Name = "panel1";
            this.panel1.Size = new System.Drawing.Size(42, 626);
            this.panel1.TabIndex = 0;
            // 
            // pictureBox1
            // 
            this.pictureBox1.Image = global::ChocoBunnyBy4KTeck__TCC_2019_.Properties.Resources.home_120px;
            this.pictureBox1.Location = new System.Drawing.Point(2, 7);
            this.pictureBox1.Name = "pictureBox1";
            this.pictureBox1.Size = new System.Drawing.Size(38, 26);
            this.pictureBox1.SizeMode = System.Windows.Forms.PictureBoxSizeMode.Zoom;
            this.pictureBox1.TabIndex = 0;
            this.pictureBox1.TabStop = false;
            this.pictureBox1.Click += new System.EventHandler(this.PictureBox1_Click);
            // 
            // PanelDosBtn
            // 
            this.PanelDosBtn.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(60)))), ((int)(((byte)(60)))), ((int)(((byte)(60)))));
            this.PanelDosBtn.Controls.Add(this.panelSuprimento);
            this.PanelDosBtn.Controls.Add(this.panelFinanceiro);
            this.PanelDosBtn.Controls.Add(this.PanelEstoque);
            this.PanelDosBtn.Controls.Add(this.panelRH);
            this.PanelDosBtn.Location = new System.Drawing.Point(42, 202);
            this.PanelDosBtn.Name = "PanelDosBtn";
            this.PanelDosBtn.Size = new System.Drawing.Size(220, 424);
            this.PanelDosBtn.TabIndex = 1;
            // 
            // panelSuprimento
            // 
            this.panelSuprimento.Controls.Add(this.bunifuFlatButton21);
            this.panelSuprimento.Controls.Add(this.bunifuFlatButton22);
            this.panelSuprimento.Controls.Add(this.bunifuFlatButton23);
            this.panelSuprimento.Controls.Add(this.btnSuprimentos);
            this.panelSuprimento.Location = new System.Drawing.Point(3, 3);
            this.panelSuprimento.MaximumSize = new System.Drawing.Size(214, 192);
            this.panelSuprimento.MinimumSize = new System.Drawing.Size(214, 47);
            this.panelSuprimento.Name = "panelSuprimento";
            this.panelSuprimento.Size = new System.Drawing.Size(214, 47);
            this.panelSuprimento.TabIndex = 14;
            // 
            // bunifuFlatButton21
            // 
            this.bunifuFlatButton21.Active = false;
            this.bunifuFlatButton21.Activecolor = System.Drawing.Color.FromArgb(((int)(((byte)(46)))), ((int)(((byte)(139)))), ((int)(((byte)(87)))));
            this.bunifuFlatButton21.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(202)))), ((int)(((byte)(53)))), ((int)(((byte)(93)))));
            this.bunifuFlatButton21.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.bunifuFlatButton21.BorderRadius = 0;
            this.bunifuFlatButton21.ButtonText = "bunifuFlatButton21";
            this.bunifuFlatButton21.Cursor = System.Windows.Forms.Cursors.Hand;
            this.bunifuFlatButton21.DisabledColor = System.Drawing.Color.Gray;
            this.bunifuFlatButton21.Dock = System.Windows.Forms.DockStyle.Top;
            this.bunifuFlatButton21.Iconcolor = System.Drawing.Color.Transparent;
            this.bunifuFlatButton21.Iconimage = null;
            this.bunifuFlatButton21.Iconimage_right = null;
            this.bunifuFlatButton21.Iconimage_right_Selected = null;
            this.bunifuFlatButton21.Iconimage_Selected = null;
            this.bunifuFlatButton21.IconMarginLeft = 0;
            this.bunifuFlatButton21.IconMarginRight = 0;
            this.bunifuFlatButton21.IconRightVisible = true;
            this.bunifuFlatButton21.IconRightZoom = 0D;
            this.bunifuFlatButton21.IconVisible = true;
            this.bunifuFlatButton21.IconZoom = 80D;
            this.bunifuFlatButton21.IsTab = false;
            this.bunifuFlatButton21.Location = new System.Drawing.Point(0, 144);
            this.bunifuFlatButton21.Name = "bunifuFlatButton21";
            this.bunifuFlatButton21.Normalcolor = System.Drawing.Color.FromArgb(((int)(((byte)(202)))), ((int)(((byte)(53)))), ((int)(((byte)(93)))));
            this.bunifuFlatButton21.OnHovercolor = System.Drawing.Color.Khaki;
            this.bunifuFlatButton21.OnHoverTextColor = System.Drawing.Color.White;
            this.bunifuFlatButton21.selected = false;
            this.bunifuFlatButton21.Size = new System.Drawing.Size(214, 48);
            this.bunifuFlatButton21.TabIndex = 16;
            this.bunifuFlatButton21.Text = "bunifuFlatButton21";
            this.bunifuFlatButton21.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            this.bunifuFlatButton21.Textcolor = System.Drawing.Color.White;
            this.bunifuFlatButton21.TextFont = new System.Drawing.Font("Neon 80s", 11F);
            // 
            // bunifuFlatButton22
            // 
            this.bunifuFlatButton22.Active = false;
            this.bunifuFlatButton22.Activecolor = System.Drawing.Color.FromArgb(((int)(((byte)(46)))), ((int)(((byte)(139)))), ((int)(((byte)(87)))));
            this.bunifuFlatButton22.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(202)))), ((int)(((byte)(53)))), ((int)(((byte)(93)))));
            this.bunifuFlatButton22.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.bunifuFlatButton22.BorderRadius = 0;
            this.bunifuFlatButton22.ButtonText = "bunifuFlatButton22";
            this.bunifuFlatButton22.Cursor = System.Windows.Forms.Cursors.Hand;
            this.bunifuFlatButton22.DisabledColor = System.Drawing.Color.Gray;
            this.bunifuFlatButton22.Dock = System.Windows.Forms.DockStyle.Top;
            this.bunifuFlatButton22.Iconcolor = System.Drawing.Color.Transparent;
            this.bunifuFlatButton22.Iconimage = null;
            this.bunifuFlatButton22.Iconimage_right = null;
            this.bunifuFlatButton22.Iconimage_right_Selected = null;
            this.bunifuFlatButton22.Iconimage_Selected = null;
            this.bunifuFlatButton22.IconMarginLeft = 0;
            this.bunifuFlatButton22.IconMarginRight = 0;
            this.bunifuFlatButton22.IconRightVisible = true;
            this.bunifuFlatButton22.IconRightZoom = 0D;
            this.bunifuFlatButton22.IconVisible = true;
            this.bunifuFlatButton22.IconZoom = 80D;
            this.bunifuFlatButton22.IsTab = false;
            this.bunifuFlatButton22.Location = new System.Drawing.Point(0, 96);
            this.bunifuFlatButton22.Name = "bunifuFlatButton22";
            this.bunifuFlatButton22.Normalcolor = System.Drawing.Color.FromArgb(((int)(((byte)(202)))), ((int)(((byte)(53)))), ((int)(((byte)(93)))));
            this.bunifuFlatButton22.OnHovercolor = System.Drawing.Color.Khaki;
            this.bunifuFlatButton22.OnHoverTextColor = System.Drawing.Color.White;
            this.bunifuFlatButton22.selected = false;
            this.bunifuFlatButton22.Size = new System.Drawing.Size(214, 48);
            this.bunifuFlatButton22.TabIndex = 15;
            this.bunifuFlatButton22.Text = "bunifuFlatButton22";
            this.bunifuFlatButton22.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            this.bunifuFlatButton22.Textcolor = System.Drawing.Color.White;
            this.bunifuFlatButton22.TextFont = new System.Drawing.Font("Neon 80s", 11F);
            // 
            // bunifuFlatButton23
            // 
            this.bunifuFlatButton23.Active = false;
            this.bunifuFlatButton23.Activecolor = System.Drawing.Color.FromArgb(((int)(((byte)(46)))), ((int)(((byte)(139)))), ((int)(((byte)(87)))));
            this.bunifuFlatButton23.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(202)))), ((int)(((byte)(53)))), ((int)(((byte)(93)))));
            this.bunifuFlatButton23.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.bunifuFlatButton23.BorderRadius = 0;
            this.bunifuFlatButton23.ButtonText = "bunifuFlatButton23";
            this.bunifuFlatButton23.Cursor = System.Windows.Forms.Cursors.Hand;
            this.bunifuFlatButton23.DisabledColor = System.Drawing.Color.Gray;
            this.bunifuFlatButton23.Dock = System.Windows.Forms.DockStyle.Top;
            this.bunifuFlatButton23.Iconcolor = System.Drawing.Color.Transparent;
            this.bunifuFlatButton23.Iconimage = null;
            this.bunifuFlatButton23.Iconimage_right = null;
            this.bunifuFlatButton23.Iconimage_right_Selected = null;
            this.bunifuFlatButton23.Iconimage_Selected = null;
            this.bunifuFlatButton23.IconMarginLeft = 0;
            this.bunifuFlatButton23.IconMarginRight = 0;
            this.bunifuFlatButton23.IconRightVisible = true;
            this.bunifuFlatButton23.IconRightZoom = 0D;
            this.bunifuFlatButton23.IconVisible = true;
            this.bunifuFlatButton23.IconZoom = 80D;
            this.bunifuFlatButton23.IsTab = false;
            this.bunifuFlatButton23.Location = new System.Drawing.Point(0, 48);
            this.bunifuFlatButton23.Name = "bunifuFlatButton23";
            this.bunifuFlatButton23.Normalcolor = System.Drawing.Color.FromArgb(((int)(((byte)(202)))), ((int)(((byte)(53)))), ((int)(((byte)(93)))));
            this.bunifuFlatButton23.OnHovercolor = System.Drawing.Color.Khaki;
            this.bunifuFlatButton23.OnHoverTextColor = System.Drawing.Color.White;
            this.bunifuFlatButton23.selected = false;
            this.bunifuFlatButton23.Size = new System.Drawing.Size(214, 48);
            this.bunifuFlatButton23.TabIndex = 14;
            this.bunifuFlatButton23.Text = "bunifuFlatButton23";
            this.bunifuFlatButton23.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            this.bunifuFlatButton23.Textcolor = System.Drawing.Color.White;
            this.bunifuFlatButton23.TextFont = new System.Drawing.Font("Neon 80s", 11F);
            // 
            // btnSuprimentos
            // 
            this.btnSuprimentos.Active = false;
            this.btnSuprimentos.Activecolor = System.Drawing.Color.Gray;
            this.btnSuprimentos.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(87)))), ((int)(((byte)(87)))), ((int)(((byte)(87)))));
            this.btnSuprimentos.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.btnSuprimentos.BorderRadius = 0;
            this.btnSuprimentos.ButtonText = "Suprimentos";
            this.btnSuprimentos.Cursor = System.Windows.Forms.Cursors.Hand;
            this.btnSuprimentos.DisabledColor = System.Drawing.Color.Gray;
            this.btnSuprimentos.Dock = System.Windows.Forms.DockStyle.Top;
            this.btnSuprimentos.Iconcolor = System.Drawing.Color.Transparent;
            this.btnSuprimentos.Iconimage = null;
            this.btnSuprimentos.Iconimage_right = global::ChocoBunnyBy4KTeck__TCC_2019_.Properties.Resources.box_120px;
            this.btnSuprimentos.Iconimage_right_Selected = null;
            this.btnSuprimentos.Iconimage_Selected = null;
            this.btnSuprimentos.IconMarginLeft = 0;
            this.btnSuprimentos.IconMarginRight = 0;
            this.btnSuprimentos.IconRightVisible = true;
            this.btnSuprimentos.IconRightZoom = 0D;
            this.btnSuprimentos.IconVisible = true;
            this.btnSuprimentos.IconZoom = 80D;
            this.btnSuprimentos.IsTab = false;
            this.btnSuprimentos.Location = new System.Drawing.Point(0, 0);
            this.btnSuprimentos.Name = "btnSuprimentos";
            this.btnSuprimentos.Normalcolor = System.Drawing.Color.FromArgb(((int)(((byte)(87)))), ((int)(((byte)(87)))), ((int)(((byte)(87)))));
            this.btnSuprimentos.OnHovercolor = System.Drawing.Color.Gray;
            this.btnSuprimentos.OnHoverTextColor = System.Drawing.Color.White;
            this.btnSuprimentos.selected = false;
            this.btnSuprimentos.Size = new System.Drawing.Size(214, 48);
            this.btnSuprimentos.TabIndex = 13;
            this.btnSuprimentos.Text = "Suprimentos";
            this.btnSuprimentos.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            this.btnSuprimentos.Textcolor = System.Drawing.Color.White;
            this.btnSuprimentos.TextFont = new System.Drawing.Font("Neon 80s", 11F);
            this.btnSuprimentos.Click += new System.EventHandler(this.BtnSuprimentos_Click);
            // 
            // panelFinanceiro
            // 
            this.panelFinanceiro.Controls.Add(this.bunifuFlatButton2);
            this.panelFinanceiro.Controls.Add(this.bunifuFlatButton3);
            this.panelFinanceiro.Controls.Add(this.bunifuFlatButton4);
            this.panelFinanceiro.Controls.Add(this.btnFinanceiro);
            this.panelFinanceiro.Location = new System.Drawing.Point(3, 56);
            this.panelFinanceiro.MaximumSize = new System.Drawing.Size(214, 192);
            this.panelFinanceiro.MinimumSize = new System.Drawing.Size(214, 47);
            this.panelFinanceiro.Name = "panelFinanceiro";
            this.panelFinanceiro.Size = new System.Drawing.Size(214, 47);
            this.panelFinanceiro.TabIndex = 15;
            // 
            // bunifuFlatButton2
            // 
            this.bunifuFlatButton2.Active = false;
            this.bunifuFlatButton2.Activecolor = System.Drawing.Color.FromArgb(((int)(((byte)(46)))), ((int)(((byte)(139)))), ((int)(((byte)(87)))));
            this.bunifuFlatButton2.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(202)))), ((int)(((byte)(53)))), ((int)(((byte)(93)))));
            this.bunifuFlatButton2.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.bunifuFlatButton2.BorderRadius = 0;
            this.bunifuFlatButton2.ButtonText = "bunifuFlatButton2";
            this.bunifuFlatButton2.Cursor = System.Windows.Forms.Cursors.Hand;
            this.bunifuFlatButton2.DisabledColor = System.Drawing.Color.Gray;
            this.bunifuFlatButton2.Dock = System.Windows.Forms.DockStyle.Top;
            this.bunifuFlatButton2.Iconcolor = System.Drawing.Color.Transparent;
            this.bunifuFlatButton2.Iconimage = null;
            this.bunifuFlatButton2.Iconimage_right = null;
            this.bunifuFlatButton2.Iconimage_right_Selected = null;
            this.bunifuFlatButton2.Iconimage_Selected = null;
            this.bunifuFlatButton2.IconMarginLeft = 0;
            this.bunifuFlatButton2.IconMarginRight = 0;
            this.bunifuFlatButton2.IconRightVisible = true;
            this.bunifuFlatButton2.IconRightZoom = 0D;
            this.bunifuFlatButton2.IconVisible = true;
            this.bunifuFlatButton2.IconZoom = 80D;
            this.bunifuFlatButton2.IsTab = false;
            this.bunifuFlatButton2.Location = new System.Drawing.Point(0, 144);
            this.bunifuFlatButton2.Name = "bunifuFlatButton2";
            this.bunifuFlatButton2.Normalcolor = System.Drawing.Color.FromArgb(((int)(((byte)(202)))), ((int)(((byte)(53)))), ((int)(((byte)(93)))));
            this.bunifuFlatButton2.OnHovercolor = System.Drawing.Color.Khaki;
            this.bunifuFlatButton2.OnHoverTextColor = System.Drawing.Color.White;
            this.bunifuFlatButton2.selected = false;
            this.bunifuFlatButton2.Size = new System.Drawing.Size(214, 48);
            this.bunifuFlatButton2.TabIndex = 16;
            this.bunifuFlatButton2.Text = "bunifuFlatButton2";
            this.bunifuFlatButton2.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            this.bunifuFlatButton2.Textcolor = System.Drawing.Color.White;
            this.bunifuFlatButton2.TextFont = new System.Drawing.Font("Neon 80s", 11F);
            // 
            // bunifuFlatButton3
            // 
            this.bunifuFlatButton3.Active = false;
            this.bunifuFlatButton3.Activecolor = System.Drawing.Color.FromArgb(((int)(((byte)(46)))), ((int)(((byte)(139)))), ((int)(((byte)(87)))));
            this.bunifuFlatButton3.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(202)))), ((int)(((byte)(53)))), ((int)(((byte)(93)))));
            this.bunifuFlatButton3.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.bunifuFlatButton3.BorderRadius = 0;
            this.bunifuFlatButton3.ButtonText = "bunifuFlatButton3";
            this.bunifuFlatButton3.Cursor = System.Windows.Forms.Cursors.Hand;
            this.bunifuFlatButton3.DisabledColor = System.Drawing.Color.Gray;
            this.bunifuFlatButton3.Dock = System.Windows.Forms.DockStyle.Top;
            this.bunifuFlatButton3.Iconcolor = System.Drawing.Color.Transparent;
            this.bunifuFlatButton3.Iconimage = null;
            this.bunifuFlatButton3.Iconimage_right = null;
            this.bunifuFlatButton3.Iconimage_right_Selected = null;
            this.bunifuFlatButton3.Iconimage_Selected = null;
            this.bunifuFlatButton3.IconMarginLeft = 0;
            this.bunifuFlatButton3.IconMarginRight = 0;
            this.bunifuFlatButton3.IconRightVisible = true;
            this.bunifuFlatButton3.IconRightZoom = 0D;
            this.bunifuFlatButton3.IconVisible = true;
            this.bunifuFlatButton3.IconZoom = 80D;
            this.bunifuFlatButton3.IsTab = false;
            this.bunifuFlatButton3.Location = new System.Drawing.Point(0, 96);
            this.bunifuFlatButton3.Name = "bunifuFlatButton3";
            this.bunifuFlatButton3.Normalcolor = System.Drawing.Color.FromArgb(((int)(((byte)(202)))), ((int)(((byte)(53)))), ((int)(((byte)(93)))));
            this.bunifuFlatButton3.OnHovercolor = System.Drawing.Color.Khaki;
            this.bunifuFlatButton3.OnHoverTextColor = System.Drawing.Color.White;
            this.bunifuFlatButton3.selected = false;
            this.bunifuFlatButton3.Size = new System.Drawing.Size(214, 48);
            this.bunifuFlatButton3.TabIndex = 15;
            this.bunifuFlatButton3.Text = "bunifuFlatButton3";
            this.bunifuFlatButton3.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            this.bunifuFlatButton3.Textcolor = System.Drawing.Color.White;
            this.bunifuFlatButton3.TextFont = new System.Drawing.Font("Neon 80s", 11F);
            // 
            // bunifuFlatButton4
            // 
            this.bunifuFlatButton4.Active = false;
            this.bunifuFlatButton4.Activecolor = System.Drawing.Color.FromArgb(((int)(((byte)(46)))), ((int)(((byte)(139)))), ((int)(((byte)(87)))));
            this.bunifuFlatButton4.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(202)))), ((int)(((byte)(53)))), ((int)(((byte)(93)))));
            this.bunifuFlatButton4.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.bunifuFlatButton4.BorderRadius = 0;
            this.bunifuFlatButton4.ButtonText = "bunifuFlatButton4";
            this.bunifuFlatButton4.Cursor = System.Windows.Forms.Cursors.Hand;
            this.bunifuFlatButton4.DisabledColor = System.Drawing.Color.Gray;
            this.bunifuFlatButton4.Dock = System.Windows.Forms.DockStyle.Top;
            this.bunifuFlatButton4.Iconcolor = System.Drawing.Color.Transparent;
            this.bunifuFlatButton4.Iconimage = null;
            this.bunifuFlatButton4.Iconimage_right = null;
            this.bunifuFlatButton4.Iconimage_right_Selected = null;
            this.bunifuFlatButton4.Iconimage_Selected = null;
            this.bunifuFlatButton4.IconMarginLeft = 0;
            this.bunifuFlatButton4.IconMarginRight = 0;
            this.bunifuFlatButton4.IconRightVisible = true;
            this.bunifuFlatButton4.IconRightZoom = 0D;
            this.bunifuFlatButton4.IconVisible = true;
            this.bunifuFlatButton4.IconZoom = 80D;
            this.bunifuFlatButton4.IsTab = false;
            this.bunifuFlatButton4.Location = new System.Drawing.Point(0, 48);
            this.bunifuFlatButton4.Name = "bunifuFlatButton4";
            this.bunifuFlatButton4.Normalcolor = System.Drawing.Color.FromArgb(((int)(((byte)(202)))), ((int)(((byte)(53)))), ((int)(((byte)(93)))));
            this.bunifuFlatButton4.OnHovercolor = System.Drawing.Color.Khaki;
            this.bunifuFlatButton4.OnHoverTextColor = System.Drawing.Color.White;
            this.bunifuFlatButton4.selected = false;
            this.bunifuFlatButton4.Size = new System.Drawing.Size(214, 48);
            this.bunifuFlatButton4.TabIndex = 14;
            this.bunifuFlatButton4.Text = "bunifuFlatButton4";
            this.bunifuFlatButton4.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            this.bunifuFlatButton4.Textcolor = System.Drawing.Color.White;
            this.bunifuFlatButton4.TextFont = new System.Drawing.Font("Neon 80s", 11F);
            // 
            // btnFinanceiro
            // 
            this.btnFinanceiro.Active = false;
            this.btnFinanceiro.Activecolor = System.Drawing.Color.Gray;
            this.btnFinanceiro.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(87)))), ((int)(((byte)(87)))), ((int)(((byte)(87)))));
            this.btnFinanceiro.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.btnFinanceiro.BorderRadius = 0;
            this.btnFinanceiro.ButtonText = "Financeiro";
            this.btnFinanceiro.Cursor = System.Windows.Forms.Cursors.Hand;
            this.btnFinanceiro.DisabledColor = System.Drawing.Color.Gray;
            this.btnFinanceiro.Dock = System.Windows.Forms.DockStyle.Top;
            this.btnFinanceiro.Iconcolor = System.Drawing.Color.Transparent;
            this.btnFinanceiro.Iconimage = null;
            this.btnFinanceiro.Iconimage_right = global::ChocoBunnyBy4KTeck__TCC_2019_.Properties.Resources.bonds_120px;
            this.btnFinanceiro.Iconimage_right_Selected = null;
            this.btnFinanceiro.Iconimage_Selected = null;
            this.btnFinanceiro.IconMarginLeft = 0;
            this.btnFinanceiro.IconMarginRight = 0;
            this.btnFinanceiro.IconRightVisible = true;
            this.btnFinanceiro.IconRightZoom = 0D;
            this.btnFinanceiro.IconVisible = true;
            this.btnFinanceiro.IconZoom = 80D;
            this.btnFinanceiro.IsTab = false;
            this.btnFinanceiro.Location = new System.Drawing.Point(0, 0);
            this.btnFinanceiro.Name = "btnFinanceiro";
            this.btnFinanceiro.Normalcolor = System.Drawing.Color.FromArgb(((int)(((byte)(87)))), ((int)(((byte)(87)))), ((int)(((byte)(87)))));
            this.btnFinanceiro.OnHovercolor = System.Drawing.Color.Gray;
            this.btnFinanceiro.OnHoverTextColor = System.Drawing.Color.White;
            this.btnFinanceiro.selected = false;
            this.btnFinanceiro.Size = new System.Drawing.Size(214, 48);
            this.btnFinanceiro.TabIndex = 13;
            this.btnFinanceiro.Text = "Financeiro";
            this.btnFinanceiro.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            this.btnFinanceiro.Textcolor = System.Drawing.Color.White;
            this.btnFinanceiro.TextFont = new System.Drawing.Font("Neon 80s", 11F);
            this.btnFinanceiro.Click += new System.EventHandler(this.BtnFinanceiro_Click);
            // 
            // PanelEstoque
            // 
            this.PanelEstoque.Controls.Add(this.bunifuFlatButton1);
            this.PanelEstoque.Controls.Add(this.bunifuFlatButton18);
            this.PanelEstoque.Controls.Add(this.bunifuFlatButton19);
            this.PanelEstoque.Controls.Add(this.btnEstoque);
            this.PanelEstoque.Location = new System.Drawing.Point(3, 109);
            this.PanelEstoque.MaximumSize = new System.Drawing.Size(214, 192);
            this.PanelEstoque.MinimumSize = new System.Drawing.Size(214, 47);
            this.PanelEstoque.Name = "PanelEstoque";
            this.PanelEstoque.Size = new System.Drawing.Size(214, 47);
            this.PanelEstoque.TabIndex = 5;
            // 
            // bunifuFlatButton1
            // 
            this.bunifuFlatButton1.Active = false;
            this.bunifuFlatButton1.Activecolor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(128)))), ((int)(((byte)(255)))));
            this.bunifuFlatButton1.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(202)))), ((int)(((byte)(53)))), ((int)(((byte)(93)))));
            this.bunifuFlatButton1.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.bunifuFlatButton1.BorderRadius = 0;
            this.bunifuFlatButton1.ButtonText = "bunifuFlatButton1";
            this.bunifuFlatButton1.Cursor = System.Windows.Forms.Cursors.Hand;
            this.bunifuFlatButton1.DisabledColor = System.Drawing.Color.Gray;
            this.bunifuFlatButton1.Dock = System.Windows.Forms.DockStyle.Top;
            this.bunifuFlatButton1.Font = new System.Drawing.Font("Altera", 8.25F);
            this.bunifuFlatButton1.Iconcolor = System.Drawing.Color.Transparent;
            this.bunifuFlatButton1.Iconimage = null;
            this.bunifuFlatButton1.Iconimage_right = null;
            this.bunifuFlatButton1.Iconimage_right_Selected = null;
            this.bunifuFlatButton1.Iconimage_Selected = null;
            this.bunifuFlatButton1.IconMarginLeft = 0;
            this.bunifuFlatButton1.IconMarginRight = 0;
            this.bunifuFlatButton1.IconRightVisible = true;
            this.bunifuFlatButton1.IconRightZoom = 0D;
            this.bunifuFlatButton1.IconVisible = true;
            this.bunifuFlatButton1.IconZoom = 80D;
            this.bunifuFlatButton1.IsTab = false;
            this.bunifuFlatButton1.Location = new System.Drawing.Point(0, 144);
            this.bunifuFlatButton1.Name = "bunifuFlatButton1";
            this.bunifuFlatButton1.Normalcolor = System.Drawing.Color.FromArgb(((int)(((byte)(202)))), ((int)(((byte)(53)))), ((int)(((byte)(93)))));
            this.bunifuFlatButton1.OnHovercolor = System.Drawing.Color.Khaki;
            this.bunifuFlatButton1.OnHoverTextColor = System.Drawing.Color.White;
            this.bunifuFlatButton1.selected = false;
            this.bunifuFlatButton1.Size = new System.Drawing.Size(214, 48);
            this.bunifuFlatButton1.TabIndex = 16;
            this.bunifuFlatButton1.Text = "bunifuFlatButton1";
            this.bunifuFlatButton1.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            this.bunifuFlatButton1.Textcolor = System.Drawing.Color.White;
            this.bunifuFlatButton1.TextFont = new System.Drawing.Font("Neon 80s", 11F);
            // 
            // bunifuFlatButton18
            // 
            this.bunifuFlatButton18.Active = false;
            this.bunifuFlatButton18.Activecolor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(128)))), ((int)(((byte)(255)))));
            this.bunifuFlatButton18.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(202)))), ((int)(((byte)(53)))), ((int)(((byte)(93)))));
            this.bunifuFlatButton18.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.bunifuFlatButton18.BorderRadius = 0;
            this.bunifuFlatButton18.ButtonText = "bunifuFlatButton18";
            this.bunifuFlatButton18.Cursor = System.Windows.Forms.Cursors.Hand;
            this.bunifuFlatButton18.DisabledColor = System.Drawing.Color.Gray;
            this.bunifuFlatButton18.Dock = System.Windows.Forms.DockStyle.Top;
            this.bunifuFlatButton18.Font = new System.Drawing.Font("Altera", 8.25F);
            this.bunifuFlatButton18.Iconcolor = System.Drawing.Color.Transparent;
            this.bunifuFlatButton18.Iconimage = null;
            this.bunifuFlatButton18.Iconimage_right = null;
            this.bunifuFlatButton18.Iconimage_right_Selected = null;
            this.bunifuFlatButton18.Iconimage_Selected = null;
            this.bunifuFlatButton18.IconMarginLeft = 0;
            this.bunifuFlatButton18.IconMarginRight = 0;
            this.bunifuFlatButton18.IconRightVisible = true;
            this.bunifuFlatButton18.IconRightZoom = 0D;
            this.bunifuFlatButton18.IconVisible = true;
            this.bunifuFlatButton18.IconZoom = 80D;
            this.bunifuFlatButton18.IsTab = false;
            this.bunifuFlatButton18.Location = new System.Drawing.Point(0, 96);
            this.bunifuFlatButton18.Name = "bunifuFlatButton18";
            this.bunifuFlatButton18.Normalcolor = System.Drawing.Color.FromArgb(((int)(((byte)(202)))), ((int)(((byte)(53)))), ((int)(((byte)(93)))));
            this.bunifuFlatButton18.OnHovercolor = System.Drawing.Color.Khaki;
            this.bunifuFlatButton18.OnHoverTextColor = System.Drawing.Color.White;
            this.bunifuFlatButton18.selected = false;
            this.bunifuFlatButton18.Size = new System.Drawing.Size(214, 48);
            this.bunifuFlatButton18.TabIndex = 15;
            this.bunifuFlatButton18.Text = "bunifuFlatButton18";
            this.bunifuFlatButton18.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            this.bunifuFlatButton18.Textcolor = System.Drawing.Color.White;
            this.bunifuFlatButton18.TextFont = new System.Drawing.Font("Neon 80s", 11F);
            // 
            // bunifuFlatButton19
            // 
            this.bunifuFlatButton19.Active = false;
            this.bunifuFlatButton19.Activecolor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(128)))), ((int)(((byte)(255)))));
            this.bunifuFlatButton19.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(202)))), ((int)(((byte)(53)))), ((int)(((byte)(93)))));
            this.bunifuFlatButton19.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.bunifuFlatButton19.BorderRadius = 0;
            this.bunifuFlatButton19.ButtonText = "bunifuFlatButton19";
            this.bunifuFlatButton19.Cursor = System.Windows.Forms.Cursors.Hand;
            this.bunifuFlatButton19.DisabledColor = System.Drawing.Color.Gray;
            this.bunifuFlatButton19.Dock = System.Windows.Forms.DockStyle.Top;
            this.bunifuFlatButton19.Font = new System.Drawing.Font("Altera", 8.25F);
            this.bunifuFlatButton19.Iconcolor = System.Drawing.Color.Transparent;
            this.bunifuFlatButton19.Iconimage = null;
            this.bunifuFlatButton19.Iconimage_right = null;
            this.bunifuFlatButton19.Iconimage_right_Selected = null;
            this.bunifuFlatButton19.Iconimage_Selected = null;
            this.bunifuFlatButton19.IconMarginLeft = 0;
            this.bunifuFlatButton19.IconMarginRight = 0;
            this.bunifuFlatButton19.IconRightVisible = true;
            this.bunifuFlatButton19.IconRightZoom = 0D;
            this.bunifuFlatButton19.IconVisible = true;
            this.bunifuFlatButton19.IconZoom = 80D;
            this.bunifuFlatButton19.IsTab = false;
            this.bunifuFlatButton19.Location = new System.Drawing.Point(0, 48);
            this.bunifuFlatButton19.Name = "bunifuFlatButton19";
            this.bunifuFlatButton19.Normalcolor = System.Drawing.Color.FromArgb(((int)(((byte)(202)))), ((int)(((byte)(53)))), ((int)(((byte)(93)))));
            this.bunifuFlatButton19.OnHovercolor = System.Drawing.Color.Khaki;
            this.bunifuFlatButton19.OnHoverTextColor = System.Drawing.Color.White;
            this.bunifuFlatButton19.selected = false;
            this.bunifuFlatButton19.Size = new System.Drawing.Size(214, 48);
            this.bunifuFlatButton19.TabIndex = 14;
            this.bunifuFlatButton19.Text = "bunifuFlatButton19";
            this.bunifuFlatButton19.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            this.bunifuFlatButton19.Textcolor = System.Drawing.Color.White;
            this.bunifuFlatButton19.TextFont = new System.Drawing.Font("Neon 80s", 11F);
            // 
            // btnEstoque
            // 
            this.btnEstoque.Active = false;
            this.btnEstoque.Activecolor = System.Drawing.Color.Gray;
            this.btnEstoque.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(87)))), ((int)(((byte)(87)))), ((int)(((byte)(87)))));
            this.btnEstoque.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.btnEstoque.BorderRadius = 0;
            this.btnEstoque.ButtonText = "Estoque";
            this.btnEstoque.Cursor = System.Windows.Forms.Cursors.Hand;
            this.btnEstoque.DisabledColor = System.Drawing.Color.Gray;
            this.btnEstoque.Dock = System.Windows.Forms.DockStyle.Top;
            this.btnEstoque.Iconcolor = System.Drawing.Color.Transparent;
            this.btnEstoque.Iconimage = null;
            this.btnEstoque.Iconimage_right = global::ChocoBunnyBy4KTeck__TCC_2019_.Properties.Resources.fork_lift_120px;
            this.btnEstoque.Iconimage_right_Selected = null;
            this.btnEstoque.Iconimage_Selected = null;
            this.btnEstoque.IconMarginLeft = 0;
            this.btnEstoque.IconMarginRight = 0;
            this.btnEstoque.IconRightVisible = true;
            this.btnEstoque.IconRightZoom = 0D;
            this.btnEstoque.IconVisible = true;
            this.btnEstoque.IconZoom = 80D;
            this.btnEstoque.IsTab = false;
            this.btnEstoque.Location = new System.Drawing.Point(0, 0);
            this.btnEstoque.Name = "btnEstoque";
            this.btnEstoque.Normalcolor = System.Drawing.Color.FromArgb(((int)(((byte)(87)))), ((int)(((byte)(87)))), ((int)(((byte)(87)))));
            this.btnEstoque.OnHovercolor = System.Drawing.Color.Gray;
            this.btnEstoque.OnHoverTextColor = System.Drawing.Color.White;
            this.btnEstoque.selected = false;
            this.btnEstoque.Size = new System.Drawing.Size(214, 48);
            this.btnEstoque.TabIndex = 13;
            this.btnEstoque.Text = "Estoque";
            this.btnEstoque.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            this.btnEstoque.Textcolor = System.Drawing.Color.White;
            this.btnEstoque.TextFont = new System.Drawing.Font("Neon 80s", 11F);
            this.btnEstoque.Click += new System.EventHandler(this.BunifuFlatButton20_Click);
            // 
            // panelRH
            // 
            this.panelRH.Controls.Add(this.bunifuFlatButton5);
            this.panelRH.Controls.Add(this.bunifuFlatButton14);
            this.panelRH.Controls.Add(this.bunifuFlatButton15);
            this.panelRH.Controls.Add(this.btnRH);
            this.panelRH.Location = new System.Drawing.Point(3, 162);
            this.panelRH.MaximumSize = new System.Drawing.Size(214, 192);
            this.panelRH.MinimumSize = new System.Drawing.Size(214, 47);
            this.panelRH.Name = "panelRH";
            this.panelRH.Size = new System.Drawing.Size(214, 47);
            this.panelRH.TabIndex = 16;
           
            // 
            // bunifuFlatButton5
            // 
            this.bunifuFlatButton5.Active = false;
            this.bunifuFlatButton5.Activecolor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(128)))), ((int)(((byte)(255)))));
            this.bunifuFlatButton5.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(202)))), ((int)(((byte)(53)))), ((int)(((byte)(93)))));
            this.bunifuFlatButton5.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.bunifuFlatButton5.BorderRadius = 0;
            this.bunifuFlatButton5.ButtonText = "bunifuFlatButton5";
            this.bunifuFlatButton5.Cursor = System.Windows.Forms.Cursors.Hand;
            this.bunifuFlatButton5.DisabledColor = System.Drawing.Color.Gray;
            this.bunifuFlatButton5.Dock = System.Windows.Forms.DockStyle.Top;
            this.bunifuFlatButton5.Iconcolor = System.Drawing.Color.Transparent;
            this.bunifuFlatButton5.Iconimage = null;
            this.bunifuFlatButton5.Iconimage_right = null;
            this.bunifuFlatButton5.Iconimage_right_Selected = null;
            this.bunifuFlatButton5.Iconimage_Selected = null;
            this.bunifuFlatButton5.IconMarginLeft = 0;
            this.bunifuFlatButton5.IconMarginRight = 0;
            this.bunifuFlatButton5.IconRightVisible = true;
            this.bunifuFlatButton5.IconRightZoom = 0D;
            this.bunifuFlatButton5.IconVisible = true;
            this.bunifuFlatButton5.IconZoom = 80D;
            this.bunifuFlatButton5.IsTab = false;
            this.bunifuFlatButton5.Location = new System.Drawing.Point(0, 144);
            this.bunifuFlatButton5.Name = "bunifuFlatButton5";
            this.bunifuFlatButton5.Normalcolor = System.Drawing.Color.FromArgb(((int)(((byte)(202)))), ((int)(((byte)(53)))), ((int)(((byte)(93)))));
            this.bunifuFlatButton5.OnHovercolor = System.Drawing.Color.Khaki;
            this.bunifuFlatButton5.OnHoverTextColor = System.Drawing.Color.White;
            this.bunifuFlatButton5.selected = false;
            this.bunifuFlatButton5.Size = new System.Drawing.Size(214, 48);
            this.bunifuFlatButton5.TabIndex = 16;
            this.bunifuFlatButton5.Text = "bunifuFlatButton5";
            this.bunifuFlatButton5.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            this.bunifuFlatButton5.Textcolor = System.Drawing.Color.White;
            this.bunifuFlatButton5.TextFont = new System.Drawing.Font("Neon 80s", 11F);
            // 
            // bunifuFlatButton14
            // 
            this.bunifuFlatButton14.Active = false;
            this.bunifuFlatButton14.Activecolor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(128)))), ((int)(((byte)(255)))));
            this.bunifuFlatButton14.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(202)))), ((int)(((byte)(53)))), ((int)(((byte)(93)))));
            this.bunifuFlatButton14.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.bunifuFlatButton14.BorderRadius = 0;
            this.bunifuFlatButton14.ButtonText = "bunifuFlatButton14";
            this.bunifuFlatButton14.Cursor = System.Windows.Forms.Cursors.Hand;
            this.bunifuFlatButton14.DisabledColor = System.Drawing.Color.Gray;
            this.bunifuFlatButton14.Dock = System.Windows.Forms.DockStyle.Top;
            this.bunifuFlatButton14.Iconcolor = System.Drawing.Color.Transparent;
            this.bunifuFlatButton14.Iconimage = null;
            this.bunifuFlatButton14.Iconimage_right = null;
            this.bunifuFlatButton14.Iconimage_right_Selected = null;
            this.bunifuFlatButton14.Iconimage_Selected = null;
            this.bunifuFlatButton14.IconMarginLeft = 0;
            this.bunifuFlatButton14.IconMarginRight = 0;
            this.bunifuFlatButton14.IconRightVisible = true;
            this.bunifuFlatButton14.IconRightZoom = 0D;
            this.bunifuFlatButton14.IconVisible = true;
            this.bunifuFlatButton14.IconZoom = 80D;
            this.bunifuFlatButton14.IsTab = false;
            this.bunifuFlatButton14.Location = new System.Drawing.Point(0, 96);
            this.bunifuFlatButton14.Name = "bunifuFlatButton14";
            this.bunifuFlatButton14.Normalcolor = System.Drawing.Color.FromArgb(((int)(((byte)(202)))), ((int)(((byte)(53)))), ((int)(((byte)(93)))));
            this.bunifuFlatButton14.OnHovercolor = System.Drawing.Color.Khaki;
            this.bunifuFlatButton14.OnHoverTextColor = System.Drawing.Color.White;
            this.bunifuFlatButton14.selected = false;
            this.bunifuFlatButton14.Size = new System.Drawing.Size(214, 48);
            this.bunifuFlatButton14.TabIndex = 15;
            this.bunifuFlatButton14.Text = "bunifuFlatButton14";
            this.bunifuFlatButton14.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            this.bunifuFlatButton14.Textcolor = System.Drawing.Color.White;
            this.bunifuFlatButton14.TextFont = new System.Drawing.Font("Neon 80s", 11F);
            // 
            // bunifuFlatButton15
            // 
            this.bunifuFlatButton15.Active = false;
            this.bunifuFlatButton15.Activecolor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(128)))), ((int)(((byte)(255)))));
            this.bunifuFlatButton15.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(202)))), ((int)(((byte)(53)))), ((int)(((byte)(93)))));
            this.bunifuFlatButton15.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.bunifuFlatButton15.BorderRadius = 0;
            this.bunifuFlatButton15.ButtonText = "Cadastro funcionario";
            this.bunifuFlatButton15.Cursor = System.Windows.Forms.Cursors.Hand;
            this.bunifuFlatButton15.DisabledColor = System.Drawing.Color.Gray;
            this.bunifuFlatButton15.Dock = System.Windows.Forms.DockStyle.Top;
            this.bunifuFlatButton15.Iconcolor = System.Drawing.Color.Transparent;
            this.bunifuFlatButton15.Iconimage = null;
            this.bunifuFlatButton15.Iconimage_right = null;
            this.bunifuFlatButton15.Iconimage_right_Selected = null;
            this.bunifuFlatButton15.Iconimage_Selected = null;
            this.bunifuFlatButton15.IconMarginLeft = 0;
            this.bunifuFlatButton15.IconMarginRight = 0;
            this.bunifuFlatButton15.IconRightVisible = true;
            this.bunifuFlatButton15.IconRightZoom = 0D;
            this.bunifuFlatButton15.IconVisible = true;
            this.bunifuFlatButton15.IconZoom = 80D;
            this.bunifuFlatButton15.IsTab = false;
            this.bunifuFlatButton15.Location = new System.Drawing.Point(0, 48);
            this.bunifuFlatButton15.Name = "bunifuFlatButton15";
            this.bunifuFlatButton15.Normalcolor = System.Drawing.Color.FromArgb(((int)(((byte)(202)))), ((int)(((byte)(53)))), ((int)(((byte)(93)))));
            this.bunifuFlatButton15.OnHovercolor = System.Drawing.Color.Khaki;
            this.bunifuFlatButton15.OnHoverTextColor = System.Drawing.Color.White;
            this.bunifuFlatButton15.selected = false;
            this.bunifuFlatButton15.Size = new System.Drawing.Size(214, 48);
            this.bunifuFlatButton15.TabIndex = 14;
            this.bunifuFlatButton15.Text = "Cadastro funcionario";
            this.bunifuFlatButton15.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            this.bunifuFlatButton15.Textcolor = System.Drawing.Color.White;
            this.bunifuFlatButton15.TextFont = new System.Drawing.Font("Neon 80s", 11F);
            this.bunifuFlatButton15.Click += new System.EventHandler(this.BunifuFlatButton15_Click);
            // 
            // btnRH
            // 
            this.btnRH.Active = false;
            this.btnRH.Activecolor = System.Drawing.Color.Gray;
            this.btnRH.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(87)))), ((int)(((byte)(87)))), ((int)(((byte)(87)))));
            this.btnRH.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.btnRH.BorderRadius = 0;
            this.btnRH.ButtonText = "R.H";
            this.btnRH.Cursor = System.Windows.Forms.Cursors.Hand;
            this.btnRH.DisabledColor = System.Drawing.Color.Gray;
            this.btnRH.Dock = System.Windows.Forms.DockStyle.Top;
            this.btnRH.Iconcolor = System.Drawing.Color.Transparent;
            this.btnRH.Iconimage = null;
            this.btnRH.Iconimage_right = global::ChocoBunnyBy4KTeck__TCC_2019_.Properties.Resources.parse_from_clipboard_120px;
            this.btnRH.Iconimage_right_Selected = null;
            this.btnRH.Iconimage_Selected = null;
            this.btnRH.IconMarginLeft = 0;
            this.btnRH.IconMarginRight = 0;
            this.btnRH.IconRightVisible = true;
            this.btnRH.IconRightZoom = 0D;
            this.btnRH.IconVisible = true;
            this.btnRH.IconZoom = 80D;
            this.btnRH.IsTab = false;
            this.btnRH.Location = new System.Drawing.Point(0, 0);
            this.btnRH.Name = "btnRH";
            this.btnRH.Normalcolor = System.Drawing.Color.FromArgb(((int)(((byte)(87)))), ((int)(((byte)(87)))), ((int)(((byte)(87)))));
            this.btnRH.OnHovercolor = System.Drawing.Color.Gray;
            this.btnRH.OnHoverTextColor = System.Drawing.Color.White;
            this.btnRH.selected = false;
            this.btnRH.Size = new System.Drawing.Size(214, 48);
            this.btnRH.TabIndex = 13;
            this.btnRH.Text = "R.H";
            this.btnRH.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            this.btnRH.Textcolor = System.Drawing.Color.White;
            this.btnRH.TextFont = new System.Drawing.Font("Neon 80s", 11F);
            this.btnRH.Click += new System.EventHandler(this.BtnRH_Click);
            // 
            // timerSuprimento
            // 
            this.timerSuprimento.Interval = 5;
            this.timerSuprimento.Tick += new System.EventHandler(this.TimerSuprimento_Tick);
            // 
            // timerFinanceiro
            // 
            this.timerFinanceiro.Interval = 5;
            this.timerFinanceiro.Tick += new System.EventHandler(this.TimerFinanceiro_Tick);
            // 
            // timerEstoque
            // 
            this.timerEstoque.Interval = 5;
            this.timerEstoque.Tick += new System.EventHandler(this.TimerEstoque_Tick);
            // 
            // timerRH
            // 
            this.timerRH.Interval = 5;
            this.timerRH.Tick += new System.EventHandler(this.TimerRH_Tick);
            // 
            // panel5
            // 
            this.panel5.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(151)))), ((int)(((byte)(151)))), ((int)(((byte)(151)))));
            this.panel5.Controls.Add(this.pictureBox3);
            this.panel5.Location = new System.Drawing.Point(0, 0);
            this.panel5.Name = "panel5";
            this.panel5.Size = new System.Drawing.Size(1259, 37);
            this.panel5.TabIndex = 3;
            // 
            // pictureBox3
            // 
            this.pictureBox3.Cursor = System.Windows.Forms.Cursors.Hand;
            this.pictureBox3.Image = global::ChocoBunnyBy4KTeck__TCC_2019_.Properties.Resources.cancel_120px;
            this.pictureBox3.Location = new System.Drawing.Point(1218, 4);
            this.pictureBox3.Name = "pictureBox3";
            this.pictureBox3.Size = new System.Drawing.Size(30, 30);
            this.pictureBox3.SizeMode = System.Windows.Forms.PictureBoxSizeMode.Zoom;
            this.pictureBox3.TabIndex = 4;
            this.pictureBox3.TabStop = false;
            this.pictureBox3.Click += new System.EventHandler(this.PictureBox3_Click);
            // 
            // MunuTimer
            // 
            this.MunuTimer.Interval = 10;
            this.MunuTimer.Tick += new System.EventHandler(this.MunuTimer_Tick);
            // 
            // panel6
            // 
            this.panel6.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(60)))), ((int)(((byte)(60)))), ((int)(((byte)(60)))));
            this.panel6.Controls.Add(this.ptrLogo);
            this.panel6.Location = new System.Drawing.Point(42, 0);
            this.panel6.Name = "panel6";
            this.panel6.Size = new System.Drawing.Size(220, 204);
            this.panel6.TabIndex = 4;
            // 
            // ptrLogo
            // 
            this.ptrLogo.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(60)))), ((int)(((byte)(60)))), ((int)(((byte)(60)))));
            this.ptrLogo.Image = global::ChocoBunnyBy4KTeck__TCC_2019_.Properties.Resources._4k_tech_logo_solo_branco;
            this.ptrLogo.Location = new System.Drawing.Point(24, 26);
            this.ptrLogo.Name = "ptrLogo";
            this.ptrLogo.Size = new System.Drawing.Size(178, 153);
            this.ptrLogo.SizeMode = System.Windows.Forms.PictureBoxSizeMode.Zoom;
            this.ptrLogo.TabIndex = 2;
            this.ptrLogo.TabStop = false;
            // 
            // frmTeste1
            // 
            this.frmTeste1.BackColor = System.Drawing.SystemColors.ControlDarkDark;
            this.frmTeste1.Location = new System.Drawing.Point(288, 65);
            this.frmTeste1.Name = "frmTeste1";
            this.frmTeste1.Size = new System.Drawing.Size(949, 510);
            this.frmTeste1.TabIndex = 6;
            // 
            // frmCadastrarfuncionario1
            // 
            this.frmCadastrarfuncionario1.BackColor = System.Drawing.SystemColors.ControlDarkDark;
            this.frmCadastrarfuncionario1.Font = new System.Drawing.Font("Neon 80s", 8.25F);
            this.frmCadastrarfuncionario1.Location = new System.Drawing.Point(288, 65);
            this.frmCadastrarfuncionario1.Name = "frmCadastrarfuncionario1";
            this.frmCadastrarfuncionario1.Size = new System.Drawing.Size(949, 510);
            this.frmCadastrarfuncionario1.TabIndex = 5;
            // 
            // frmMenu
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(23)))), ((int)(((byte)(23)))), ((int)(((byte)(23)))));
            this.ClientSize = new System.Drawing.Size(1255, 625);
            this.Controls.Add(this.frmTeste1);
            this.Controls.Add(this.frmCadastrarfuncionario1);
            this.Controls.Add(this.panel6);
            this.Controls.Add(this.panel1);
            this.Controls.Add(this.PanelDosBtn);
            this.Controls.Add(this.panel5);
            this.Font = new System.Drawing.Font("Altera", 8.25F);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.None;
            this.Name = "frmMenu";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "frmLogin";
            this.Load += new System.EventHandler(this.FrmMenu_Load);
            this.panel1.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox1)).EndInit();
            this.PanelDosBtn.ResumeLayout(false);
            this.panelSuprimento.ResumeLayout(false);
            this.panelFinanceiro.ResumeLayout(false);
            this.PanelEstoque.ResumeLayout(false);
            this.panelRH.ResumeLayout(false);
            this.panel5.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox3)).EndInit();
            this.panel6.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.ptrLogo)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.Panel panel1;
        private System.Windows.Forms.PictureBox pictureBox1;
        private System.Windows.Forms.FlowLayoutPanel PanelDosBtn;
        private System.Windows.Forms.PictureBox ptrLogo;
        private System.Windows.Forms.Timer timerSuprimento;
        private System.Windows.Forms.Timer timerFinanceiro;
        private System.Windows.Forms.Timer timerEstoque;
        private System.Windows.Forms.Timer timerRH;
        private System.Windows.Forms.Panel panel5;
        private System.Windows.Forms.PictureBox pictureBox3;
        private System.Windows.Forms.Timer MunuTimer;
        private System.Windows.Forms.Panel panel6;
        private System.Windows.Forms.Panel PanelEstoque;
        private Bunifu.Framework.UI.BunifuFlatButton bunifuFlatButton1;
        private Bunifu.Framework.UI.BunifuFlatButton bunifuFlatButton18;
        private Bunifu.Framework.UI.BunifuFlatButton bunifuFlatButton19;
        private Bunifu.Framework.UI.BunifuFlatButton btnEstoque;
        private System.Windows.Forms.Panel panelSuprimento;
        private System.Windows.Forms.Panel panelFinanceiro;
        private System.Windows.Forms.Panel panelRH;
        private Bunifu.Framework.UI.BunifuFlatButton bunifuFlatButton21;
        private Bunifu.Framework.UI.BunifuFlatButton bunifuFlatButton22;
        private Bunifu.Framework.UI.BunifuFlatButton bunifuFlatButton23;
        private Bunifu.Framework.UI.BunifuFlatButton btnSuprimentos;
        private Bunifu.Framework.UI.BunifuFlatButton bunifuFlatButton2;
        private Bunifu.Framework.UI.BunifuFlatButton bunifuFlatButton3;
        private Bunifu.Framework.UI.BunifuFlatButton bunifuFlatButton4;
        private Bunifu.Framework.UI.BunifuFlatButton btnFinanceiro;
        private Bunifu.Framework.UI.BunifuFlatButton bunifuFlatButton5;
        private Bunifu.Framework.UI.BunifuFlatButton bunifuFlatButton14;
        private Bunifu.Framework.UI.BunifuFlatButton bunifuFlatButton15;
        private Bunifu.Framework.UI.BunifuFlatButton btnRH;
        private funcionarios.frmCadastrarfuncionario frmCadastrarfuncionario1;
        private frmTeste frmTeste1;
    }
}