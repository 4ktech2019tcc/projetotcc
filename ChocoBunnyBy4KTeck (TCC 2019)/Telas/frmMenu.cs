﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace ChocoBunnyBy4KTeck__TCC_2019_.Telas
{
    public partial class frmMenu : Form
    {
        private bool isCollapsed;
        public frmMenu()
        {
            InitializeComponent();
        }

        //btn 10
        

        private void MunuTimer_Tick(object sender, EventArgs e)
        {
            if (isCollapsed)
            {

                panel1.Width += 10;
                if (panel1.Size == panel1.MaximumSize)
                {
                    MunuTimer.Stop();
                    isCollapsed = false;
                }
            }
            else
            {

                panel1.Width -= 20;
                if (panel1.Size == panel1.MinimumSize)
                {
                    MunuTimer.Stop();
                    isCollapsed = true;
                    PanelDosBtn.Show();
                    ptrLogo.Show();
                    panel6.Show();
                }
            }
        }

        private void PictureBox1_Click(object sender, EventArgs e)
        {
            PanelDosBtn.Hide();
            ptrLogo.Hide();
            panel6.Hide();
            MunuTimer.Start();
        }

       

        private void TimerSuprimento_Tick(object sender, EventArgs e)
        {
            if (isCollapsed)
            {

                panelSuprimento.Height += 10;
                if (panelSuprimento.Size == panelSuprimento.MaximumSize)
                {
                    timerSuprimento.Stop();
                    isCollapsed = false;
                }
            }
            else
            {

                panelSuprimento.Height -= 10;
                if (panelSuprimento.Size == panelSuprimento.MinimumSize)
                {
                    timerSuprimento.Stop();
                    isCollapsed = true;
                    
                }
            }
        }

       

        private void TimerEstoque_Tick(object sender, EventArgs e)
        {
            if (isCollapsed)
            {

                PanelEstoque.Height += 10;
                if (PanelEstoque.Size == PanelEstoque.MaximumSize)
                {
                    timerEstoque.Stop();
                    isCollapsed = false;
                }
            }
            else
            {

                PanelEstoque.Height -= 10;
                if (PanelEstoque.Size == PanelEstoque.MinimumSize)
                {
                    timerEstoque.Stop();
                    isCollapsed = true;
                    
                }
            }
        }

        private void BtnSuprimentos_Click(object sender, EventArgs e)
        {
            timerSuprimento.Start();
           
        }

        private void BunifuFlatButton20_Click(object sender, EventArgs e)
        {
            timerEstoque.Start();
        }

        private void TimerFinanceiro_Tick(object sender, EventArgs e)
        {
            if (isCollapsed)
            {

                panelFinanceiro.Height += 10;
                if (panelFinanceiro.Size == panelFinanceiro.MaximumSize)
                {
                    timerFinanceiro.Stop();
                    isCollapsed = false;
                }
            }
            else
            {

                panelFinanceiro.Height -= 10;
                if (panelFinanceiro.Size == panelFinanceiro.MinimumSize)
                {
                    timerFinanceiro.Stop();
                    isCollapsed = true;

                }
            }
        }

        private void TimerRH_Tick(object sender, EventArgs e)
        {
            if (isCollapsed)
            {

                panelRH.Height += 10;
                if (panelRH.Size == panelRH.MaximumSize)
                {
                    timerRH.Stop();
                    isCollapsed = false;
                }
            }
            else
            {

                panelRH.Height -= 10;
                if (panelRH.Size == panelRH.MinimumSize)
                {
                    timerRH.Stop();
                    isCollapsed = true;

                }
            }
        }

        private void BtnFinanceiro_Click(object sender, EventArgs e)
        {
            timerFinanceiro.Start();
            frmTeste1.Show();
            frmTeste1.BringToFront();
        }

        private void BtnRH_Click(object sender, EventArgs e)
        {
            timerRH.Start();
        }
                         
                 

        private void FrmMenu_Load(object sender, EventArgs e)
        {
            frmTeste1.Hide();
            frmCadastrarfuncionario1.Hide();
            
        }

        private void PictureBox3_Click(object sender, EventArgs e)
        {
            Application.Exit();
        }

        private void BunifuFlatButton15_Click(object sender, EventArgs e)
        {
            frmCadastrarfuncionario1.Show();
            frmCadastrarfuncionario1.BringToFront();
        }
              
    }
}
               
                
        
