﻿namespace ChocoBunnyBy4KTeck__TCC_2019_.Telas.funcionarios
{
    partial class frmCadastrarfuncionario
    {
        /// <summary> 
        /// Variável de designer necessária.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary> 
        /// Limpar os recursos que estão sendo usados.
        /// </summary>
        /// <param name="disposing">true se for necessário descartar os recursos gerenciados; caso contrário, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Código gerado pelo Designer de Componentes

        /// <summary> 
        /// Método necessário para suporte ao Designer - não modifique 
        /// o conteúdo deste método com o editor de código.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(frmCadastrarfuncionario));
            this.groupBox2 = new System.Windows.Forms.GroupBox();
            this.bunifuSwitch7 = new Bunifu.Framework.UI.BunifuSwitch();
            this.bunifuSwitch5 = new Bunifu.Framework.UI.BunifuSwitch();
            this.bunifuSwitch6 = new Bunifu.Framework.UI.BunifuSwitch();
            this.bunifuSwitch3 = new Bunifu.Framework.UI.BunifuSwitch();
            this.bunifuSwitch4 = new Bunifu.Framework.UI.BunifuSwitch();
            this.bunifuSwitch2 = new Bunifu.Framework.UI.BunifuSwitch();
            this.bunifuSwitch1 = new Bunifu.Framework.UI.BunifuSwitch();
            this.txtSenha = new System.Windows.Forms.TextBox();
            this.ckbAdmin = new System.Windows.Forms.CheckBox();
            this.ckbRH = new System.Windows.Forms.CheckBox();
            this.ckbCompras = new System.Windows.Forms.CheckBox();
            this.ckbVenda = new System.Windows.Forms.CheckBox();
            this.ckbLogistica = new System.Windows.Forms.CheckBox();
            this.ckbFinanceiro = new System.Windows.Forms.CheckBox();
            this.ckbFuncionario = new System.Windows.Forms.CheckBox();
            this.label18 = new System.Windows.Forms.Label();
            this.txtuser = new System.Windows.Forms.TextBox();
            this.label19 = new System.Windows.Forms.Label();
            this.groupBox1 = new System.Windows.Forms.GroupBox();
            this.numericUpDown5 = new System.Windows.Forms.NumericUpDown();
            this.nudValeRefeiçao = new System.Windows.Forms.NumericUpDown();
            this.nudValeAlimentaçao = new System.Windows.Forms.NumericUpDown();
            this.txtEndereço = new System.Windows.Forms.TextBox();
            this.label9 = new System.Windows.Forms.Label();
            this.nudValeTranporte = new System.Windows.Forms.NumericUpDown();
            this.nudSalario = new System.Windows.Forms.NumericUpDown();
            this.nudConvenio = new System.Windows.Forms.Label();
            this.label5 = new System.Windows.Forms.Label();
            this.label4 = new System.Windows.Forms.Label();
            this.label3 = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.label8 = new System.Windows.Forms.Label();
            this.txtNome = new System.Windows.Forms.TextBox();
            this.label12 = new System.Windows.Forms.Label();
            this.txtCPF = new System.Windows.Forms.MaskedTextBox();
            this.label11 = new System.Windows.Forms.Label();
            this.txtRG = new System.Windows.Forms.MaskedTextBox();
            this.bunifuFlatButton1 = new Bunifu.Framework.UI.BunifuFlatButton();
            this.groupBox2.SuspendLayout();
            this.groupBox1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.numericUpDown5)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.nudValeRefeiçao)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.nudValeAlimentaçao)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.nudValeTranporte)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.nudSalario)).BeginInit();
            this.SuspendLayout();
            // 
            // groupBox2
            // 
            this.groupBox2.BackColor = System.Drawing.Color.Transparent;
            this.groupBox2.Controls.Add(this.bunifuSwitch7);
            this.groupBox2.Controls.Add(this.bunifuSwitch5);
            this.groupBox2.Controls.Add(this.bunifuSwitch6);
            this.groupBox2.Controls.Add(this.bunifuSwitch3);
            this.groupBox2.Controls.Add(this.bunifuSwitch4);
            this.groupBox2.Controls.Add(this.bunifuSwitch2);
            this.groupBox2.Controls.Add(this.bunifuSwitch1);
            this.groupBox2.Controls.Add(this.txtSenha);
            this.groupBox2.Controls.Add(this.ckbAdmin);
            this.groupBox2.Controls.Add(this.ckbRH);
            this.groupBox2.Controls.Add(this.ckbCompras);
            this.groupBox2.Controls.Add(this.ckbVenda);
            this.groupBox2.Controls.Add(this.ckbLogistica);
            this.groupBox2.Controls.Add(this.ckbFinanceiro);
            this.groupBox2.Controls.Add(this.ckbFuncionario);
            this.groupBox2.Controls.Add(this.label18);
            this.groupBox2.Controls.Add(this.txtuser);
            this.groupBox2.Controls.Add(this.label19);
            this.groupBox2.Font = new System.Drawing.Font("Neon 80s", 11.25F);
            this.groupBox2.ForeColor = System.Drawing.Color.White;
            this.groupBox2.Location = new System.Drawing.Point(489, 33);
            this.groupBox2.Name = "groupBox2";
            this.groupBox2.Size = new System.Drawing.Size(372, 398);
            this.groupBox2.TabIndex = 121;
            this.groupBox2.TabStop = false;
            this.groupBox2.Text = "Acesso";
            this.groupBox2.Enter += new System.EventHandler(this.GroupBox2_Enter);
            // 
            // bunifuSwitch7
            // 
            this.bunifuSwitch7.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(64)))), ((int)(((byte)(64)))), ((int)(((byte)(64)))));
            this.bunifuSwitch7.BorderRadius = 0;
            this.bunifuSwitch7.Cursor = System.Windows.Forms.Cursors.Hand;
            this.bunifuSwitch7.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(224)))), ((int)(((byte)(224)))), ((int)(((byte)(224)))));
            this.bunifuSwitch7.Location = new System.Drawing.Point(219, 271);
            this.bunifuSwitch7.Margin = new System.Windows.Forms.Padding(9, 6, 9, 6);
            this.bunifuSwitch7.Name = "bunifuSwitch7";
            this.bunifuSwitch7.Oncolor = System.Drawing.Color.SeaGreen;
            this.bunifuSwitch7.Onoffcolor = System.Drawing.Color.DarkGray;
            this.bunifuSwitch7.Size = new System.Drawing.Size(51, 19);
            this.bunifuSwitch7.TabIndex = 107;
            this.bunifuSwitch7.Textcolor = System.Drawing.Color.FromArgb(((int)(((byte)(224)))), ((int)(((byte)(224)))), ((int)(((byte)(224)))));
            this.bunifuSwitch7.Value = true;
            // 
            // bunifuSwitch5
            // 
            this.bunifuSwitch5.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(64)))), ((int)(((byte)(64)))), ((int)(((byte)(64)))));
            this.bunifuSwitch5.BorderRadius = 0;
            this.bunifuSwitch5.Cursor = System.Windows.Forms.Cursors.Hand;
            this.bunifuSwitch5.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(224)))), ((int)(((byte)(224)))), ((int)(((byte)(224)))));
            this.bunifuSwitch5.Location = new System.Drawing.Point(219, 218);
            this.bunifuSwitch5.Margin = new System.Windows.Forms.Padding(9, 6, 9, 6);
            this.bunifuSwitch5.Name = "bunifuSwitch5";
            this.bunifuSwitch5.Oncolor = System.Drawing.Color.SeaGreen;
            this.bunifuSwitch5.Onoffcolor = System.Drawing.Color.DarkGray;
            this.bunifuSwitch5.Size = new System.Drawing.Size(51, 19);
            this.bunifuSwitch5.TabIndex = 105;
            this.bunifuSwitch5.Textcolor = System.Drawing.Color.FromArgb(((int)(((byte)(224)))), ((int)(((byte)(224)))), ((int)(((byte)(224)))));
            this.bunifuSwitch5.Value = true;
            // 
            // bunifuSwitch6
            // 
            this.bunifuSwitch6.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(64)))), ((int)(((byte)(64)))), ((int)(((byte)(64)))));
            this.bunifuSwitch6.BorderRadius = 0;
            this.bunifuSwitch6.Cursor = System.Windows.Forms.Cursors.Hand;
            this.bunifuSwitch6.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(224)))), ((int)(((byte)(224)))), ((int)(((byte)(224)))));
            this.bunifuSwitch6.Location = new System.Drawing.Point(219, 244);
            this.bunifuSwitch6.Margin = new System.Windows.Forms.Padding(6, 5, 6, 5);
            this.bunifuSwitch6.Name = "bunifuSwitch6";
            this.bunifuSwitch6.Oncolor = System.Drawing.Color.SeaGreen;
            this.bunifuSwitch6.Onoffcolor = System.Drawing.Color.DarkGray;
            this.bunifuSwitch6.Size = new System.Drawing.Size(51, 19);
            this.bunifuSwitch6.TabIndex = 106;
            this.bunifuSwitch6.Textcolor = System.Drawing.Color.FromArgb(((int)(((byte)(224)))), ((int)(((byte)(224)))), ((int)(((byte)(224)))));
            this.bunifuSwitch6.Value = true;
            // 
            // bunifuSwitch3
            // 
            this.bunifuSwitch3.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(64)))), ((int)(((byte)(64)))), ((int)(((byte)(64)))));
            this.bunifuSwitch3.BorderRadius = 0;
            this.bunifuSwitch3.Cursor = System.Windows.Forms.Cursors.Hand;
            this.bunifuSwitch3.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(224)))), ((int)(((byte)(224)))), ((int)(((byte)(224)))));
            this.bunifuSwitch3.Location = new System.Drawing.Point(219, 165);
            this.bunifuSwitch3.Margin = new System.Windows.Forms.Padding(9, 6, 9, 6);
            this.bunifuSwitch3.Name = "bunifuSwitch3";
            this.bunifuSwitch3.Oncolor = System.Drawing.Color.SeaGreen;
            this.bunifuSwitch3.Onoffcolor = System.Drawing.Color.DarkGray;
            this.bunifuSwitch3.Size = new System.Drawing.Size(51, 19);
            this.bunifuSwitch3.TabIndex = 103;
            this.bunifuSwitch3.Textcolor = System.Drawing.Color.FromArgb(((int)(((byte)(224)))), ((int)(((byte)(224)))), ((int)(((byte)(224)))));
            this.bunifuSwitch3.Value = true;
            // 
            // bunifuSwitch4
            // 
            this.bunifuSwitch4.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(64)))), ((int)(((byte)(64)))), ((int)(((byte)(64)))));
            this.bunifuSwitch4.BorderRadius = 0;
            this.bunifuSwitch4.Cursor = System.Windows.Forms.Cursors.Hand;
            this.bunifuSwitch4.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(224)))), ((int)(((byte)(224)))), ((int)(((byte)(224)))));
            this.bunifuSwitch4.Location = new System.Drawing.Point(219, 192);
            this.bunifuSwitch4.Margin = new System.Windows.Forms.Padding(6, 5, 6, 5);
            this.bunifuSwitch4.Name = "bunifuSwitch4";
            this.bunifuSwitch4.Oncolor = System.Drawing.Color.SeaGreen;
            this.bunifuSwitch4.Onoffcolor = System.Drawing.Color.DarkGray;
            this.bunifuSwitch4.Size = new System.Drawing.Size(51, 19);
            this.bunifuSwitch4.TabIndex = 104;
            this.bunifuSwitch4.Textcolor = System.Drawing.Color.FromArgb(((int)(((byte)(224)))), ((int)(((byte)(224)))), ((int)(((byte)(224)))));
            this.bunifuSwitch4.Value = true;
            // 
            // bunifuSwitch2
            // 
            this.bunifuSwitch2.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(64)))), ((int)(((byte)(64)))), ((int)(((byte)(64)))));
            this.bunifuSwitch2.BorderRadius = 0;
            this.bunifuSwitch2.Cursor = System.Windows.Forms.Cursors.Hand;
            this.bunifuSwitch2.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(224)))), ((int)(((byte)(224)))), ((int)(((byte)(224)))));
            this.bunifuSwitch2.Location = new System.Drawing.Point(219, 102);
            this.bunifuSwitch2.Margin = new System.Windows.Forms.Padding(6, 5, 6, 5);
            this.bunifuSwitch2.Name = "bunifuSwitch2";
            this.bunifuSwitch2.Oncolor = System.Drawing.Color.SeaGreen;
            this.bunifuSwitch2.Onoffcolor = System.Drawing.Color.DarkGray;
            this.bunifuSwitch2.Size = new System.Drawing.Size(51, 19);
            this.bunifuSwitch2.TabIndex = 102;
            this.bunifuSwitch2.Textcolor = System.Drawing.Color.FromArgb(((int)(((byte)(224)))), ((int)(((byte)(224)))), ((int)(((byte)(224)))));
            this.bunifuSwitch2.Value = true;
            // 
            // bunifuSwitch1
            // 
            this.bunifuSwitch1.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(64)))), ((int)(((byte)(64)))), ((int)(((byte)(64)))));
            this.bunifuSwitch1.BorderRadius = 0;
            this.bunifuSwitch1.Cursor = System.Windows.Forms.Cursors.Hand;
            this.bunifuSwitch1.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(224)))), ((int)(((byte)(224)))), ((int)(((byte)(224)))));
            this.bunifuSwitch1.Location = new System.Drawing.Point(219, 133);
            this.bunifuSwitch1.Margin = new System.Windows.Forms.Padding(4);
            this.bunifuSwitch1.Name = "bunifuSwitch1";
            this.bunifuSwitch1.Oncolor = System.Drawing.Color.SeaGreen;
            this.bunifuSwitch1.Onoffcolor = System.Drawing.Color.DarkGray;
            this.bunifuSwitch1.Size = new System.Drawing.Size(51, 19);
            this.bunifuSwitch1.TabIndex = 102;
            this.bunifuSwitch1.Textcolor = System.Drawing.Color.FromArgb(((int)(((byte)(224)))), ((int)(((byte)(224)))), ((int)(((byte)(224)))));
            this.bunifuSwitch1.Value = true;
            // 
            // txtSenha
            // 
            this.txtSenha.Font = new System.Drawing.Font("Neon 80s", 12F);
            this.txtSenha.Location = new System.Drawing.Point(92, 50);
            this.txtSenha.MaxLength = 100;
            this.txtSenha.Name = "txtSenha";
            this.txtSenha.Size = new System.Drawing.Size(259, 26);
            this.txtSenha.TabIndex = 101;
            this.txtSenha.UseSystemPasswordChar = true;
            // 
            // ckbAdmin
            // 
            this.ckbAdmin.AutoSize = true;
            this.ckbAdmin.Location = new System.Drawing.Point(92, 102);
            this.ckbAdmin.Name = "ckbAdmin";
            this.ckbAdmin.Size = new System.Drawing.Size(75, 20);
            this.ckbAdmin.TabIndex = 12;
            this.ckbAdmin.Text = "Admin";
            this.ckbAdmin.UseVisualStyleBackColor = true;
            // 
            // ckbRH
            // 
            this.ckbRH.AutoSize = true;
            this.ckbRH.Location = new System.Drawing.Point(92, 166);
            this.ckbRH.Name = "ckbRH";
            this.ckbRH.Size = new System.Drawing.Size(46, 20);
            this.ckbRH.TabIndex = 14;
            this.ckbRH.Text = "RH";
            this.ckbRH.UseVisualStyleBackColor = true;
            // 
            // ckbCompras
            // 
            this.ckbCompras.AutoSize = true;
            this.ckbCompras.Location = new System.Drawing.Point(92, 192);
            this.ckbCompras.Name = "ckbCompras";
            this.ckbCompras.Size = new System.Drawing.Size(92, 20);
            this.ckbCompras.TabIndex = 15;
            this.ckbCompras.Text = "Compras";
            this.ckbCompras.UseVisualStyleBackColor = true;
            // 
            // ckbVenda
            // 
            this.ckbVenda.AutoSize = true;
            this.ckbVenda.Location = new System.Drawing.Point(92, 243);
            this.ckbVenda.Name = "ckbVenda";
            this.ckbVenda.Size = new System.Drawing.Size(81, 20);
            this.ckbVenda.TabIndex = 17;
            this.ckbVenda.Text = "Vendas";
            this.ckbVenda.UseVisualStyleBackColor = true;
            // 
            // ckbLogistica
            // 
            this.ckbLogistica.AutoSize = true;
            this.ckbLogistica.Location = new System.Drawing.Point(92, 218);
            this.ckbLogistica.Name = "ckbLogistica";
            this.ckbLogistica.Size = new System.Drawing.Size(90, 20);
            this.ckbLogistica.TabIndex = 16;
            this.ckbLogistica.Text = "Logística";
            this.ckbLogistica.UseVisualStyleBackColor = true;
            // 
            // ckbFinanceiro
            // 
            this.ckbFinanceiro.AutoSize = true;
            this.ckbFinanceiro.Location = new System.Drawing.Point(92, 270);
            this.ckbFinanceiro.Name = "ckbFinanceiro";
            this.ckbFinanceiro.Size = new System.Drawing.Size(100, 20);
            this.ckbFinanceiro.TabIndex = 18;
            this.ckbFinanceiro.Text = "Financeiro";
            this.ckbFinanceiro.UseVisualStyleBackColor = true;
            // 
            // ckbFuncionario
            // 
            this.ckbFuncionario.AutoSize = true;
            this.ckbFuncionario.Location = new System.Drawing.Point(92, 133);
            this.ckbFuncionario.Name = "ckbFuncionario";
            this.ckbFuncionario.Size = new System.Drawing.Size(110, 20);
            this.ckbFuncionario.TabIndex = 13;
            this.ckbFuncionario.Text = "Funcionário";
            this.ckbFuncionario.UseVisualStyleBackColor = true;
            // 
            // label18
            // 
            this.label18.AutoSize = true;
            this.label18.BackColor = System.Drawing.Color.Transparent;
            this.label18.Font = new System.Drawing.Font("Neon 80s", 12F);
            this.label18.ForeColor = System.Drawing.Color.White;
            this.label18.Location = new System.Drawing.Point(24, 23);
            this.label18.Name = "label18";
            this.label18.Size = new System.Drawing.Size(50, 16);
            this.label18.TabIndex = 71;
            this.label18.Text = "Login:";
            // 
            // txtuser
            // 
            this.txtuser.Font = new System.Drawing.Font("Neon 80s", 12F);
            this.txtuser.Location = new System.Drawing.Point(92, 20);
            this.txtuser.MaxLength = 100;
            this.txtuser.Multiline = true;
            this.txtuser.Name = "txtuser";
            this.txtuser.Size = new System.Drawing.Size(259, 24);
            this.txtuser.TabIndex = 10;
            // 
            // label19
            // 
            this.label19.AutoSize = true;
            this.label19.BackColor = System.Drawing.Color.Transparent;
            this.label19.Font = new System.Drawing.Font("Neon 80s", 12F);
            this.label19.ForeColor = System.Drawing.Color.White;
            this.label19.Location = new System.Drawing.Point(21, 50);
            this.label19.Name = "label19";
            this.label19.Size = new System.Drawing.Size(56, 16);
            this.label19.TabIndex = 100;
            this.label19.Text = "Senha:";
            // 
            // groupBox1
            // 
            this.groupBox1.BackColor = System.Drawing.Color.Transparent;
            this.groupBox1.Controls.Add(this.numericUpDown5);
            this.groupBox1.Controls.Add(this.nudValeRefeiçao);
            this.groupBox1.Controls.Add(this.nudValeAlimentaçao);
            this.groupBox1.Controls.Add(this.txtEndereço);
            this.groupBox1.Controls.Add(this.label9);
            this.groupBox1.Controls.Add(this.nudValeTranporte);
            this.groupBox1.Controls.Add(this.nudSalario);
            this.groupBox1.Controls.Add(this.nudConvenio);
            this.groupBox1.Controls.Add(this.label5);
            this.groupBox1.Controls.Add(this.label4);
            this.groupBox1.Controls.Add(this.label3);
            this.groupBox1.Controls.Add(this.label2);
            this.groupBox1.Controls.Add(this.label8);
            this.groupBox1.Controls.Add(this.txtNome);
            this.groupBox1.Controls.Add(this.label12);
            this.groupBox1.Controls.Add(this.txtCPF);
            this.groupBox1.Controls.Add(this.label11);
            this.groupBox1.Controls.Add(this.txtRG);
            this.groupBox1.Font = new System.Drawing.Font("Neon 80s", 12F);
            this.groupBox1.ForeColor = System.Drawing.Color.White;
            this.groupBox1.Location = new System.Drawing.Point(67, 33);
            this.groupBox1.Name = "groupBox1";
            this.groupBox1.Size = new System.Drawing.Size(398, 398);
            this.groupBox1.TabIndex = 120;
            this.groupBox1.TabStop = false;
            this.groupBox1.Text = "Funcionário";
            this.groupBox1.Enter += new System.EventHandler(this.GroupBox1_Enter);
            // 
            // numericUpDown5
            // 
            this.numericUpDown5.DecimalPlaces = 2;
            this.numericUpDown5.Font = new System.Drawing.Font("Neon 80s", 12F);
            this.numericUpDown5.Location = new System.Drawing.Point(114, 315);
            this.numericUpDown5.Maximum = new decimal(new int[] {
            100000,
            0,
            0,
            0});
            this.numericUpDown5.Name = "numericUpDown5";
            this.numericUpDown5.Size = new System.Drawing.Size(130, 26);
            this.numericUpDown5.TabIndex = 9;
            // 
            // nudValeRefeiçao
            // 
            this.nudValeRefeiçao.DecimalPlaces = 2;
            this.nudValeRefeiçao.Font = new System.Drawing.Font("Neon 80s", 12F);
            this.nudValeRefeiçao.Location = new System.Drawing.Point(114, 281);
            this.nudValeRefeiçao.Maximum = new decimal(new int[] {
            100000,
            0,
            0,
            0});
            this.nudValeRefeiçao.Name = "nudValeRefeiçao";
            this.nudValeRefeiçao.Size = new System.Drawing.Size(130, 26);
            this.nudValeRefeiçao.TabIndex = 8;
            // 
            // nudValeAlimentaçao
            // 
            this.nudValeAlimentaçao.DecimalPlaces = 2;
            this.nudValeAlimentaçao.Font = new System.Drawing.Font("Neon 80s", 12F);
            this.nudValeAlimentaçao.Location = new System.Drawing.Point(114, 249);
            this.nudValeAlimentaçao.Maximum = new decimal(new int[] {
            1000000,
            0,
            0,
            0});
            this.nudValeAlimentaçao.Name = "nudValeAlimentaçao";
            this.nudValeAlimentaçao.Size = new System.Drawing.Size(130, 26);
            this.nudValeAlimentaçao.TabIndex = 7;
            // 
            // txtEndereço
            // 
            this.txtEndereço.Font = new System.Drawing.Font("Neon 80s", 12F);
            this.txtEndereço.Location = new System.Drawing.Point(114, 52);
            this.txtEndereço.MaxLength = 150;
            this.txtEndereço.Multiline = true;
            this.txtEndereço.Name = "txtEndereço";
            this.txtEndereço.Size = new System.Drawing.Size(259, 24);
            this.txtEndereço.TabIndex = 4;
            // 
            // label9
            // 
            this.label9.AutoSize = true;
            this.label9.BackColor = System.Drawing.Color.Transparent;
            this.label9.Font = new System.Drawing.Font("Neon 80s", 12F);
            this.label9.ForeColor = System.Drawing.Color.White;
            this.label9.Location = new System.Drawing.Point(11, 56);
            this.label9.Name = "label9";
            this.label9.Size = new System.Drawing.Size(80, 16);
            this.label9.TabIndex = 100;
            this.label9.Text = "Endereço:";
            // 
            // nudValeTranporte
            // 
            this.nudValeTranporte.DecimalPlaces = 2;
            this.nudValeTranporte.Font = new System.Drawing.Font("Neon 80s", 12F);
            this.nudValeTranporte.Location = new System.Drawing.Point(113, 217);
            this.nudValeTranporte.Maximum = new decimal(new int[] {
            1000000,
            0,
            0,
            0});
            this.nudValeTranporte.Name = "nudValeTranporte";
            this.nudValeTranporte.Size = new System.Drawing.Size(131, 26);
            this.nudValeTranporte.TabIndex = 6;
            // 
            // nudSalario
            // 
            this.nudSalario.DecimalPlaces = 2;
            this.nudSalario.Font = new System.Drawing.Font("Neon 80s", 12F);
            this.nudSalario.Location = new System.Drawing.Point(113, 183);
            this.nudSalario.Maximum = new decimal(new int[] {
            1000000,
            0,
            0,
            0});
            this.nudSalario.Name = "nudSalario";
            this.nudSalario.Size = new System.Drawing.Size(131, 26);
            this.nudSalario.TabIndex = 5;
            // 
            // nudConvenio
            // 
            this.nudConvenio.AutoSize = true;
            this.nudConvenio.BackColor = System.Drawing.Color.Transparent;
            this.nudConvenio.Font = new System.Drawing.Font("Neon 80s", 12F);
            this.nudConvenio.ForeColor = System.Drawing.Color.White;
            this.nudConvenio.Location = new System.Drawing.Point(62, 315);
            this.nudConvenio.Name = "nudConvenio";
            this.nudConvenio.Size = new System.Drawing.Size(40, 16);
            this.nudConvenio.TabIndex = 116;
            this.nudConvenio.Text = "Cnv:";
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.BackColor = System.Drawing.Color.Transparent;
            this.label5.Font = new System.Drawing.Font("Neon 80s", 12F);
            this.label5.ForeColor = System.Drawing.Color.White;
            this.label5.Location = new System.Drawing.Point(72, 281);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(34, 16);
            this.label5.TabIndex = 114;
            this.label5.Text = "VR:";
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.BackColor = System.Drawing.Color.Transparent;
            this.label4.Font = new System.Drawing.Font("Neon 80s", 12F);
            this.label4.ForeColor = System.Drawing.Color.White;
            this.label4.Location = new System.Drawing.Point(73, 247);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(37, 16);
            this.label4.TabIndex = 112;
            this.label4.Text = "VA:";
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.BackColor = System.Drawing.Color.Transparent;
            this.label3.Font = new System.Drawing.Font("Neon 80s", 12F);
            this.label3.ForeColor = System.Drawing.Color.White;
            this.label3.Location = new System.Drawing.Point(72, 217);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(33, 16);
            this.label3.TabIndex = 110;
            this.label3.Text = "VT:";
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.BackColor = System.Drawing.Color.Transparent;
            this.label2.Font = new System.Drawing.Font("Neon 80s", 12F);
            this.label2.ForeColor = System.Drawing.Color.White;
            this.label2.Location = new System.Drawing.Point(47, 186);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(59, 16);
            this.label2.TabIndex = 108;
            this.label2.Text = "Salário:";
            // 
            // label8
            // 
            this.label8.AutoSize = true;
            this.label8.BackColor = System.Drawing.Color.Transparent;
            this.label8.Font = new System.Drawing.Font("Neon 80s", 12F);
            this.label8.ForeColor = System.Drawing.Color.White;
            this.label8.Location = new System.Drawing.Point(47, 26);
            this.label8.Name = "label8";
            this.label8.Size = new System.Drawing.Size(56, 16);
            this.label8.TabIndex = 71;
            this.label8.Text = "Nome:";
            // 
            // txtNome
            // 
            this.txtNome.Font = new System.Drawing.Font("Neon 80s", 12F);
            this.txtNome.Location = new System.Drawing.Point(114, 22);
            this.txtNome.MaxLength = 150;
            this.txtNome.Multiline = true;
            this.txtNome.Name = "txtNome";
            this.txtNome.Size = new System.Drawing.Size(259, 24);
            this.txtNome.TabIndex = 1;
            this.txtNome.TextChanged += new System.EventHandler(this.TxtNome_TextChanged);
            // 
            // label12
            // 
            this.label12.AutoSize = true;
            this.label12.BackColor = System.Drawing.Color.Transparent;
            this.label12.Font = new System.Drawing.Font("Neon 80s", 12F);
            this.label12.ForeColor = System.Drawing.Color.White;
            this.label12.Location = new System.Drawing.Point(62, 89);
            this.label12.Name = "label12";
            this.label12.Size = new System.Drawing.Size(40, 16);
            this.label12.TabIndex = 74;
            this.label12.Text = "CPF:";
            // 
            // txtCPF
            // 
            this.txtCPF.Font = new System.Drawing.Font("Neon 80s", 12F);
            this.txtCPF.Location = new System.Drawing.Point(113, 83);
            this.txtCPF.Mask = "000,000,000-00";
            this.txtCPF.Name = "txtCPF";
            this.txtCPF.Size = new System.Drawing.Size(131, 26);
            this.txtCPF.TabIndex = 2;
            // 
            // label11
            // 
            this.label11.AutoSize = true;
            this.label11.BackColor = System.Drawing.Color.Transparent;
            this.label11.Font = new System.Drawing.Font("Neon 80s", 12F);
            this.label11.ForeColor = System.Drawing.Color.White;
            this.label11.Location = new System.Drawing.Point(70, 121);
            this.label11.Name = "label11";
            this.label11.Size = new System.Drawing.Size(34, 16);
            this.label11.TabIndex = 73;
            this.label11.Text = "RG:";
            // 
            // txtRG
            // 
            this.txtRG.Font = new System.Drawing.Font("Neon 80s", 12F);
            this.txtRG.Location = new System.Drawing.Point(112, 115);
            this.txtRG.Mask = "00,000,000-0";
            this.txtRG.Name = "txtRG";
            this.txtRG.Size = new System.Drawing.Size(132, 26);
            this.txtRG.TabIndex = 3;
            this.txtRG.TextMaskFormat = System.Windows.Forms.MaskFormat.IncludePromptAndLiterals;
            // 
            // bunifuFlatButton1
            // 
            this.bunifuFlatButton1.Active = false;
            this.bunifuFlatButton1.Activecolor = System.Drawing.Color.FromArgb(((int)(((byte)(46)))), ((int)(((byte)(139)))), ((int)(((byte)(87)))));
            this.bunifuFlatButton1.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(202)))), ((int)(((byte)(53)))), ((int)(((byte)(93)))));
            this.bunifuFlatButton1.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.bunifuFlatButton1.BorderRadius = 0;
            this.bunifuFlatButton1.ButtonText = "Cadastrar";
            this.bunifuFlatButton1.Cursor = System.Windows.Forms.Cursors.Hand;
            this.bunifuFlatButton1.DisabledColor = System.Drawing.Color.Gray;
            this.bunifuFlatButton1.Iconcolor = System.Drawing.Color.Transparent;
            this.bunifuFlatButton1.Iconimage = ((System.Drawing.Image)(resources.GetObject("bunifuFlatButton1.Iconimage")));
            this.bunifuFlatButton1.Iconimage_right = null;
            this.bunifuFlatButton1.Iconimage_right_Selected = null;
            this.bunifuFlatButton1.Iconimage_Selected = null;
            this.bunifuFlatButton1.IconMarginLeft = 0;
            this.bunifuFlatButton1.IconMarginRight = 0;
            this.bunifuFlatButton1.IconRightVisible = true;
            this.bunifuFlatButton1.IconRightZoom = 0D;
            this.bunifuFlatButton1.IconVisible = true;
            this.bunifuFlatButton1.IconZoom = 90D;
            this.bunifuFlatButton1.IsTab = false;
            this.bunifuFlatButton1.Location = new System.Drawing.Point(67, 437);
            this.bunifuFlatButton1.Name = "bunifuFlatButton1";
            this.bunifuFlatButton1.Normalcolor = System.Drawing.Color.FromArgb(((int)(((byte)(202)))), ((int)(((byte)(53)))), ((int)(((byte)(93)))));
            this.bunifuFlatButton1.OnHovercolor = System.Drawing.Color.FromArgb(((int)(((byte)(202)))), ((int)(((byte)(53)))), ((int)(((byte)(93)))));
            this.bunifuFlatButton1.OnHoverTextColor = System.Drawing.Color.White;
            this.bunifuFlatButton1.selected = false;
            this.bunifuFlatButton1.Size = new System.Drawing.Size(794, 48);
            this.bunifuFlatButton1.TabIndex = 122;
            this.bunifuFlatButton1.Text = "Cadastrar";
            this.bunifuFlatButton1.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            this.bunifuFlatButton1.Textcolor = System.Drawing.Color.White;
            this.bunifuFlatButton1.TextFont = new System.Drawing.Font("Neon 80s", 12F);
            this.bunifuFlatButton1.Click += new System.EventHandler(this.BunifuFlatButton1_Click);
            // 
            // frmCadastrarfuncionario
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 11F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.SystemColors.ControlDarkDark;
            this.Controls.Add(this.bunifuFlatButton1);
            this.Controls.Add(this.groupBox2);
            this.Controls.Add(this.groupBox1);
            this.Font = new System.Drawing.Font("Neon 80s", 8.25F);
            this.Name = "frmCadastrarfuncionario";
            this.Size = new System.Drawing.Size(949, 510);
            this.groupBox2.ResumeLayout(false);
            this.groupBox2.PerformLayout();
            this.groupBox1.ResumeLayout(false);
            this.groupBox1.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.numericUpDown5)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.nudValeRefeiçao)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.nudValeAlimentaçao)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.nudValeTranporte)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.nudSalario)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.GroupBox groupBox2;
        private System.Windows.Forms.TextBox txtSenha;
        private System.Windows.Forms.CheckBox ckbAdmin;
        private System.Windows.Forms.CheckBox ckbRH;
        private System.Windows.Forms.CheckBox ckbCompras;
        private System.Windows.Forms.CheckBox ckbVenda;
        private System.Windows.Forms.CheckBox ckbLogistica;
        private System.Windows.Forms.CheckBox ckbFinanceiro;
        private System.Windows.Forms.CheckBox ckbFuncionario;
        private System.Windows.Forms.Label label18;
        private System.Windows.Forms.TextBox txtuser;
        private System.Windows.Forms.Label label19;
        private System.Windows.Forms.GroupBox groupBox1;
        private System.Windows.Forms.NumericUpDown numericUpDown5;
        private System.Windows.Forms.NumericUpDown nudValeRefeiçao;
        private System.Windows.Forms.NumericUpDown nudValeAlimentaçao;
        private System.Windows.Forms.NumericUpDown nudValeTranporte;
        private System.Windows.Forms.NumericUpDown nudSalario;
        private System.Windows.Forms.Label nudConvenio;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Label label8;
        private System.Windows.Forms.TextBox txtNome;
        private System.Windows.Forms.Label label9;
        private System.Windows.Forms.TextBox txtEndereço;
        private System.Windows.Forms.Label label12;
        private System.Windows.Forms.MaskedTextBox txtCPF;
        private System.Windows.Forms.Label label11;
        private System.Windows.Forms.MaskedTextBox txtRG;
        private Bunifu.Framework.UI.BunifuFlatButton bunifuFlatButton1;
        private Bunifu.Framework.UI.BunifuSwitch bunifuSwitch7;
        private Bunifu.Framework.UI.BunifuSwitch bunifuSwitch5;
        private Bunifu.Framework.UI.BunifuSwitch bunifuSwitch6;
        private Bunifu.Framework.UI.BunifuSwitch bunifuSwitch3;
        private Bunifu.Framework.UI.BunifuSwitch bunifuSwitch4;
        private Bunifu.Framework.UI.BunifuSwitch bunifuSwitch2;
        private Bunifu.Framework.UI.BunifuSwitch bunifuSwitch1;
    }
}
